#ifndef _LINUX_QUAD_DSP_H
#define _LINUX_QUAD_DSP_H 1

#include <linux/compiler.h>
#include <linux/miscdevice.h>
#include <linux/types.h>

#define PREFIX      "QuadDSP: "
#define REMIX_BSIZE ((size_t)(64 * 1024))

struct file;

enum {
	EL_PRIVATE = 1,
	EL_BUFS,
	EL_FILP_FRONT,
	EL_FILP_REAR,
	EL_NO_FILTER,
	EL_NORMAL,
};

struct prv {
	struct filter *filter;
	ssize_t (*write)(struct file *, const char __user *, size_t, loff_t *);
	/* Basics */
	struct file *front, *rear;
	int channels, warning;
	/* For fx_dxr/fx_dpl2 */
	unsigned int fmt;
	/* For fx_split */
	void *buf_front, *buf_rear;
	unsigned int chans_front, chans_rear, bpc;
};

struct filter {
	ssize_t (*write_dual)(struct file *, const char __user *, size_t, loff_t *);
	ssize_t (*write_plus)(struct file *, const char __user *, size_t, loff_t *);
	ssize_t (*write_mono)(struct file *, const char __user *, size_t, loff_t *);
	struct miscdevice misc_dev;
	int misc_reg;
};

/*
 *	QDSP_DEV.C
 */
extern unsigned int Swap_bank_x, Swap_bank_z;

/*
 *	  CODECS
 */
ssize_t fx_dup(struct file *, const char __user *, size_t, loff_t *);
ssize_t fx_split(struct file *, const char __user *, size_t, loff_t *);

extern struct filter Qfilter_diff, Qfilter_dpl2, Qfilter_dsr, Qfilter_dup,
                     Qfilter_dxr, Qfilter_qs, Qfilter_split, Qfilter_sq;

/*
 *	INLINE FUNCTIONS
 */
static inline long pv_squash8(long v)
{
	if (v < -128) return -128;
	if (v >  127) return  127;
	return v;
}

static inline long pv_squash16(long v)
{
	if (v < -32768) return -32768;
	if (v >  32767) return  32767;
	return v;
}

static inline void *select_z_front(struct prv *p)
{
	return !Swap_bank_z ? p->buf_front : p->buf_rear;
}

static inline void *select_z_rear(struct prv *p)
{
	return !Swap_bank_z ? p->buf_rear : p->buf_front;
}

#endif /* _LINUX_QUAD_DSP_H */
