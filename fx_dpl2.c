/*
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2006 - 2009
 *
 *	This file is part of quad_dsp. quad_dsp is free software; you
 *	can redistribute it and/or modify it under the terms of the
 *	GNU General Public License as published by the Free Software
 *	Foundation; either version 2 or 3 of the License.
 */
#include <linux/compiler.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/soundcard.h>
#include <linux/types.h>
#include <asm/uaccess.h>
#include "quad_dsp.h"

#ifdef INTEGER_ARITH
#	ifndef PRECISION_4
#		define REAR_LEFT(l, r)  ((-8164 * (l) - 5773 * (r)) / 10000)
#		define REAR_RIGHT(l, r) ((5773 * (l) + 8164 * (r)) / 10000)
#	else
#		define REAR_LEFT(l, r)  ((-26755 * (l) - 18918 * (r)) / 32768)
#		define REAR_RIGHT(l, r) ((18918 * (l) + 26755 * (r)) / 32768)
#	endif
#else
#	define sqrt __builtin_sqrt
#	define REAR_LEFT(l, r)  (sqrt(2.0/3) * (l) + -sqrt(1.0/3) * (r))
#	define REAR_RIGHT(l, r) (sqrt(1.0/3) * (l) + -sqrt(2.0/3) * (r))
#endif

static ssize_t fx_dpl2(struct file *filp, const char __user *buf, size_t count,
    loff_t *ppos)
{
	struct prv *p = filp->private_data;
	int eax = 0, ebx = 0;
	mm_segment_t oldfs;
	loff_t pos = 0;
	unsigned int max = min(count, REMIX_BSIZE),
	             front_idx = 0, rear_idx = 0;

	if (count > max && !p->warning) {
		printk(KERN_WARNING PREFIX "%s: pid %d wants to write %u "
		       "bytes, but there are only %u free\n", __FUNCTION__,
		       current->pid, count, max);
		++p->warning;
	}

	if (copy_from_user(select_z_front(p), buf, max) != 0)
		return -EFAULT;

	if (p->fmt == AFMT_S16_LE) {
		int16_t *pf = select_z_front(p), *pr = select_z_rear(p);
		int32_t l, r;
		max /= 2;

		while (front_idx < max) {
			l = pf[front_idx++];
			r = pf[front_idx++];
			pr[rear_idx++] = pv_squash16(REAR_LEFT(l, r));
			pr[rear_idx++] = pv_squash16(REAR_RIGHT(l, r));
		}

		front_idx *= 2;
		rear_idx  *= 2;
	} else if (p->fmt == AFMT_S8) {
		int8_t *pf = select_z_front(p), *pr = select_z_rear(p);
		int16_t l, r;

		while (front_idx < max) {
			l = pf[front_idx++];
			r = pf[front_idx++];
			pr[rear_idx++] = pv_squash8(REAR_LEFT(l, r));
			pr[rear_idx++] = pv_squash8(REAR_RIGHT(l, r));
		}
	} else if (p->fmt == AFMT_U8) {
		uint8_t *pf = select_z_front(p), *pr = select_z_rear(p);
		int16_t l, r;

		while (front_idx < max) {
			l = pf[front_idx++] - 128;
			r = pf[front_idx++] - 128;
			pr[rear_idx++] = pv_squash8(REAR_LEFT(l, r) + 128);
			pr[rear_idx++] = pv_squash8(REAR_RIGHT(l, r) + 128);
		}
	} else {
		return -EINVAL;
	}

	oldfs = get_fs();
	set_fs(KERNEL_DS);

	if ((eax = vfs_write(p->front, p->buf_front, front_idx, &pos)) < 0) {
		set_fs(oldfs);
		return eax;
	}
	pos = 0;
	if ((ebx = vfs_write(p->rear, p->buf_rear, rear_idx, &pos)) < 0) {
		set_fs(oldfs);
		return ebx;
	}

	set_fs(oldfs);
	return (eax > ebx) ? eax : ebx;
}

struct filter Qfilter_dpl2 = {
	.write_mono = fx_dup,
	.write_dual = fx_dpl2,
	.write_plus = fx_split,
	.misc_dev   = {.name = "Qdsp_dpl2"},
};
