/*
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2009
 *
 *	This file is part of quad_dsp. quad_dsp is free software; you
 *	can redistribute it and/or modify it under the terms of the
 *	GNU General Public License as published by the Free Software
 *	Foundation; either version 2 or 3 of the License.
 */
#include <linux/compiler.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/soundcard.h>
#include <linux/types.h>
#include <asm/uaccess.h>
#include "quad_dsp.h"

static ssize_t fx_qs(struct file *filp, const char __user *buf,
    size_t count, loff_t *ppos)
{
	struct prv *p = filp->private_data;
	int eax = 0, ebx = 0;
	mm_segment_t oldfs;
	loff_t pos = 0;
	unsigned int max = min(count, REMIX_BSIZE),
	             front_idx = 0, rear_idx = 0;

	if (count > max && !p->warning) {
		printk(KERN_WARNING PREFIX "%s: pid %d wants to write %u "
		       "bytes, but there are only %u free\n", __FUNCTION__,
		       current->pid, count, max);
		++p->warning;
	}

	if (copy_from_user(select_z_front(p), buf, max) != 0)
		return -EFAULT;

	if (p->fmt == AFMT_S16_LE) {
		int16_t *pf = select_z_front(p), *pr = select_z_rear(p);
		long left, right;
		max /= 2;

		while (front_idx < max) {
			left            = pf[front_idx++];
			right           = pf[front_idx++];
			pr[rear_idx++]  = pv_squash16(left * 92 / 100 - right * 38 / 100);
			pr[rear_idx++]  = pv_squash16(left * 38 / 100 - right * 92 / 100);
		}

		front_idx *= 2;
		rear_idx  *= 2;
	} else {
		return -EINVAL;
	}

	oldfs = get_fs();
	set_fs(KERNEL_DS);

	if ((eax = vfs_write(p->front, p->buf_front, front_idx, &pos)) < 0) {
		set_fs(oldfs);
		return eax;
	}
	pos = 0;
	if ((ebx = vfs_write(p->rear, p->buf_rear, rear_idx, &pos)) < 0) {
		set_fs(oldfs);
		return ebx;
	}

	set_fs(oldfs);
	return (eax > ebx) ? eax : ebx;
}

struct filter Qfilter_qs = {
	.write_mono = fx_dup,
	.write_dual = fx_qs,
	.write_plus = fx_qs,
	.misc_dev   = {.name = "Qdsp_qs"},
};
