# -*- Makefile -*-

MODULES_DIR := /lib/modules/$(shell uname -r)
KERNEL_DIR  := ${MODULES_DIR}/build

.PHONY: all modules modules_install install clean

all:
	make -C "${KERNEL_DIR}" M="$$PWD" "$@";

modules_install:
	make -C "${KERNEL_DIR}" M="$$PWD" "$@";

clean:
	make -C "${KERNEL_DIR}" M="$$PWD" "$@";

obj-m         += quad_dsp.o
quad_dsp-objs := qdsp_dev.o fx_diff.o fx_dpl2.o fx_dsr.o fx_dup.o \
                 fx_dxr.o fx_qs.o fx_split.o fx_sq.o

# Pick one of the following:
EXTRA_CFLAGS += -mno-soft-float
#EXTRA_CFLAGS += -DINTEGER_ARITH
