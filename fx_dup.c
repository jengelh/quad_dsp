/*
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2006 - 2008
 *
 *	This file is part of quad_dsp. quad_dsp is free software; you
 *	can redistribute it and/or modify it under the terms of the
 *	GNU General Public License as published by the Free Software
 *	Foundation; either version 2 or 3 of the License.
 */
#include <linux/compiler.h>
#include <linux/fs.h>
#include <linux/types.h>
#include "quad_dsp.h"

ssize_t fx_dup(struct file *filp, const char __user *buf, size_t count,
    loff_t *ppos)
{
	struct prv *p = filp->private_data;
	int eax = 0, ebx = 0;
	loff_t pos = 0;

	/* If one write failed, do not send the data to the other. We do not want
	to make it worse, if something broke. */
	if ((eax = vfs_write(p->front, buf, count, &pos)) < 0)
		return eax;
	pos = 0;
	if ((ebx = vfs_write(p->rear, buf, count, &pos)) < 0)
		return ebx;

	return (eax > ebx) ? eax : ebx;
}

struct filter Qfilter_dup = {
	.write_mono = fx_dup,
	.write_dual = fx_dup,
	.write_plus = fx_split,
	.misc_dev   = {.name = "Qdsp_dup"},
};
