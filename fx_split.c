/*
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2006 - 2008
 *
 *	This file is part of quad_dsp. quad_dsp is free software; you
 *	can redistribute it and/or modify it under the terms of the
 *	GNU General Public License as published by the Free Software
 *	Foundation; either version 2 or 3 of the License.
 */
#include <linux/compiler.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <asm/uaccess.h>
#include "quad_dsp.h"

ssize_t fx_split(struct file *filp, const char __user *buf, size_t count,
    loff_t *ppos)
{
	struct prv *p = filp->private_data;
	int eax = 0, ebx = 0;
	mm_segment_t oldfs;
	loff_t pos = 0;

	unsigned char *pf = select_z_front(p), *pr = select_z_rear(p);
	unsigned int j, walker = 0, max = min(count, REMIX_BSIZE),
	             front_idx = 0, rear_idx = 0,
	             bytes_front = p->bpc * p->chans_front,
	             bytes_rear  = p->bpc * p->chans_rear;

	if (count > max && !p->warning) {
		printk(KERN_WARNING PREFIX "%s: pid %d wants to write %u "
		       "bytes, but there are only %u free\n", __FUNCTION__,
		       current->pid, count, max);
		++p->warning;
	}

	if (copy_from_user(pf, buf, max) != 0)
		return -EFAULT;

	/* Take a 4-channel stream and disassemble it into 2x 2-ch. */
	while (walker < max) {
		for (j = 0; j < bytes_front; ++j)
			pf[front_idx++] = pf[walker++];
		for (j = 0; j < bytes_rear; ++j)
			pr[rear_idx++] = pf[walker++];
	}

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	if ((eax = vfs_write(p->front, pf, front_idx, &pos)) < 0) {
		set_fs(oldfs);
		return eax;
	}
	pos = 0;
	if ((ebx = vfs_write(p->rear, pr, rear_idx, &pos)) < 0) {
		set_fs(oldfs);
		return ebx;
	}

	set_fs(oldfs);
	return (eax > ebx) ? eax : ebx;
}
