/*
 *	QuadDSP - 4-channel audio output tools
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2006 - 2008
 *	v1.6.1, June 2008
 *	http://jengelh.medozas.de/projects/quad_dsp/
 *
 *	This file is part of quad_dsp. quad_dsp is free software; you
 *	can redistribute it and/or modify it under the terms of the
 *	GNU General Public License as published by the Free Software
 *	Foundation; either version 2 or 3 of the License.
 */
#include <linux/compiler.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kdev_t.h>
#include <linux/kernel.h>
#include <linux/limits.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <linux/smp_lock.h> /* lock_kernel() */
#include <linux/soundcard.h>
#include <linux/types.h>
#include <linux/vmalloc.h>
#include <asm/pgtable.h> /* PAGE_KERNEL */
#include <asm/uaccess.h>
#include "quad_dsp.h"

#define DEFAULT_DSP  "/dev/dsp"
#define DEFAULT_ADSP "/dev/adsp"

static struct filter *const Filter_list[] = {
	&Qfilter_diff,
	&Qfilter_dup,
	&Qfilter_dsr,
	&Qfilter_dxr,
	&Qfilter_dpl2,
	&Qfilter_qs,
	&Qfilter_sq,
	NULL,
};

static char Front_device[NAME_MAX] = DEFAULT_DSP,
            Rear_device[NAME_MAX]  = DEFAULT_ADSP;
unsigned int Swap_bank_z;

static struct filter *filter_find(dev_t d)
{
	struct filter *const *f = Filter_list;
	while (*f != NULL) {
		if (d == MKDEV(MISC_MAJOR, (**f).misc_dev.minor))
			return *f;
		++f;
	}
	return NULL;
}

static void qdsp_close_internal(struct prv *p, unsigned int level)
{
	switch (level) { /* full fallthrough */
		case EL_NORMAL:
		case EL_NO_FILTER:
			filp_close(p->rear, NULL);
		case EL_FILP_REAR:
			filp_close(p->front, NULL);
		case EL_FILP_FRONT:
		case EL_BUFS:
			vfree(p->buf_rear);
			vfree(p->buf_front);
		case EL_PRIVATE:
			kfree(p);
	}
	return;
}

static int qdsp_open(struct inode *inode, struct file *filp)
{
	struct prv *p;
	int ret = -ENOMEM;
	dev_t dev;

	if ((p = kmalloc(sizeof(struct prv), GFP_USER)) == NULL)
		return -ENOMEM;

	p->buf_front = vmalloc(REMIX_BSIZE);
	p->buf_rear  = vmalloc(REMIX_BSIZE);
	if (p->buf_front == NULL || p->buf_rear == NULL) {
		qdsp_close_internal(p, EL_BUFS);
		return -ENOMEM;
	}

	/*
	 * Open underlying devices. No file descriptors are used, so that the
	 * fd table of the process is not unnecessarily enlarged (by factor 3).
	 */
	if (IS_ERR(p->front = filp_open(Front_device, O_WRONLY, 0222)) ||
	    unlikely(p->front == NULL)) {
		printk(KERN_WARNING PREFIX "Could not open front device "
		       "%s: %d\n", Front_device, ret);
		qdsp_close_internal(p, EL_FILP_FRONT);
		return PTR_ERR(p->front);
	}
	if (IS_ERR(p->rear = filp_open(Rear_device, O_WRONLY, 0222)) ||
	    unlikely(p->rear == NULL)) {
		printk(KERN_WARNING PREFIX "Could not open rear device "
		       "%s: %d\n", Rear_device, ret);
		qdsp_close_internal(p, EL_FILP_REAR);
		return PTR_ERR(p->rear);
	}

	/* Default is 8000 Hz, 8 bit per sample, 1 channel (=> fx_dup) */
	dev = filp->f_dentry->d_inode->i_rdev;
	if ((p->filter = filter_find(dev)) != NULL) {
		p->write = p->filter->write_mono;
	} else {
		printk(KERN_WARNING PREFIX "No filter associated with device"
		       " %d:%d!\n", MAJOR(dev), MINOR(dev));
		qdsp_close_internal(p, EL_NO_FILTER);
		return -ENXIO;
	}

	p->warning     = 0;
	p->fmt         = AFMT_U8;
	p->bpc         = 1;
	p->chans_front = 1;
	p->chans_rear  = 0;
	filp->private_data = p;
	return 0;
}

static ssize_t qdsp_write(struct file *filp, const char __user *buf,
    size_t count, loff_t *ppos)
{
	struct prv *p = filp->private_data;
	if (p->write == NULL) {
		printk(KERN_WARNING PREFIX "Whoops, p->write == NULL. "
		       "Forgot to *fully* implement a filter?\n");
		dump_stack();
		return -EIO;
	}
	return p->write(filp, buf, count, ppos);
}

static long do_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Copy of fs/ioctl.c:do_ioctl() -- because it is static there */
	long ret = 0;
	if (filp->f_op->unlocked_ioctl != NULL) {
		ret = filp->f_op->unlocked_ioctl(filp, cmd, arg);
		if (ret == -ENOIOCTLCMD)
			ret = -EINVAL;
		return ret;
	}
	if (filp->f_op->ioctl != NULL) {
		lock_kernel();
		ret = filp->f_op->ioctl(filp->f_dentry->d_inode,
		                        filp, cmd, arg);
		unlock_kernel();
	}
	return ret;
}

static long qdsp_ioctl_channels(struct file *filp, unsigned long arg)
{
	struct prv *p = filp->private_data;
	int eax = 0, ebx = 0, ch = -1;

	if (get_user(ch, (int __user *)arg))
		return -EFAULT;

	if (ch < 1) {
		return -EINVAL;
	} else if (ch == 1) {
		eax = do_ioctl(p->front, SNDCTL_DSP_CHANNELS, arg);
		ebx = do_ioctl(p->rear,  SNDCTL_DSP_CHANNELS, arg);
		p->write       = p->filter->write_mono;
		p->chans_front = 1;
		p->chans_rear  = 0;
	} else if (ch == 2) {
		eax = do_ioctl(p->front, SNDCTL_DSP_CHANNELS, arg);
		ebx = do_ioctl(p->rear,  SNDCTL_DSP_CHANNELS, arg);
		p->write       = p->filter->write_dual;
		p->chans_front = 2;
		p->chans_rear  = 0;
	} else {
		/*
		 * Select 4-to-2x2 channel split. Integer divisions always
		 * truncate to zero, so @b (rear) is calculated first and @a
		 * gets the most channels, should @orig be an odd number.
		 */
		int b = ch / 2, a = ch - b;
		mm_segment_t oldfs = get_fs();

		set_fs(KERNEL_DS);
		eax = do_ioctl(p->front, SNDCTL_DSP_CHANNELS, (unsigned long)&a);
		ebx = do_ioctl(p->rear,  SNDCTL_DSP_CHANNELS, (unsigned long)&b);
		set_fs(oldfs);

		p->write       = p->filter->write_plus;
		p->chans_front = a;
		p->chans_rear  = b;
	}

	p->channels = ch;
	if (eax < 0)
		return eax;
	if (ebx < 0)
		return ebx;
	return (eax < ebx) ? eax : ebx;
}

static long qdsp_ioctl(struct file *filp, unsigned int cmd,
    unsigned long arg)
{
	struct prv *p = filp->private_data;
	long eax = 0, ebx = 0;

	if (cmd == SNDCTL_DSP_CHANNELS) {
		return qdsp_ioctl_channels(filp, arg);
	} else if (cmd == SNDCTL_DSP_STEREO) {
		int orig, ch;
		if (get_user(orig, (int __user *)arg) != 0)
			return -EFAULT;
		ch = 1 + orig;
		if (put_user(ch, (int __user *)arg) != 0)
			return -EFAULT;
		eax = qdsp_ioctl_channels(filp, arg);
		put_user(orig, (int __user *)arg);
		return eax;
	} else if (cmd == SNDCTL_DSP_SETFMT) {
		/* Only accept a bunch of formats. Too lazy to code. */
		int fmt;
		if (get_user(fmt, (int __user *)arg) != 0)
			return -EFAULT;

		/* SETFMT includes SAMPLESIZE... */
		p->fmt = fmt;
		switch (fmt) {
			case AFMT_U8:
			case AFMT_S8:
				p->bpc = 8;
				break;
			case AFMT_S16_LE:
				p->bpc = 16;
				break;
			default:
				return -EINVAL;
		}
	} else if (cmd == SNDCTL_DSP_SAMPLESIZE) {
		/*
		 * Only accept multiples of 8, so I do not need to code the
		 * paths for odd-bits. Nobody uses odds effectively anyway.
		 *
		 * I seem to remember that something went wrong without
		 * volatile. Can't say right now..
		 */
		volatile int v = 8, w;
		if (get_user(v, (int __user *)arg) != 0)
			return -EFAULT;
		w = v / 8;
		if (w * 8 != v)
			return -EINVAL;
		p->bpc = w;
	}

	eax = do_ioctl(p->front, cmd, arg);
	ebx = do_ioctl(p->rear,  cmd, arg);
	if (eax < 0)
		return eax;
	if (ebx < 0)
		return ebx;
	return (eax < ebx) ? eax : ebx;
}

static int qdsp_close(struct inode *inode, struct file *filp)
{
	qdsp_close_internal(filp->private_data, EL_NORMAL);
	return 0;
}

static const struct file_operations Qdsp_fops = {
	.open           = qdsp_open,
	.write          = qdsp_write,
	.unlocked_ioctl = qdsp_ioctl,
	.release        = qdsp_close,
	.owner          = THIS_MODULE,
};

static __init int filter_register(struct filter *f)
{
	int ret;
	f->misc_dev.minor = MISC_DYNAMIC_MINOR;
	f->misc_dev.fops  = &Qdsp_fops;
	ret = misc_register(&f->misc_dev);
	if (ret < 0)
		return f->misc_reg = ret;
	if (ret == 0)
		return f->misc_reg = 1;
	return f->misc_reg > 0;
}

static void quad_dsp_exit(void)
{
	struct filter *const *f = Filter_list;
	for (f = Filter_list; *f != NULL; ++f)
		if ((*f)->misc_reg > 0)
			misc_deregister(&(*f)->misc_dev);
	return;
}

static __init int quad_dsp_init(void)
{
	struct filter *const *f = Filter_list;
	for (f = Filter_list; *f != NULL; ++f) {
		if (filter_register(*f) <= 0) {
			quad_dsp_exit();
			printk(KERN_ERR PREFIX "Error during registration "
			       "of %s: %d\n", (*f)->misc_dev.name,
			       (*f)->misc_reg);
			return (*f)->misc_reg;
		}
	}
	return 0;
}

module_init(quad_dsp_init);
module_exit(quad_dsp_exit);
MODULE_DESCRIPTION("QuadDSP, 4-channel audio output tools");
MODULE_AUTHOR("Jan Engelhardt <jengelh@medozas.de>");
MODULE_LICENSE("GPL");
module_param_string(front, Front_device, sizeof(Front_device), S_IRUGO | S_IWUSR);
module_param_string(rear,  Rear_device, sizeof(Rear_device), S_IRUGO | S_IWUSR);
module_param_named(zswap, Swap_bank_z, uint, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(front, "Front device (default: " DEFAULT_DSP ")");
MODULE_PARM_DESC(rear,  "Rear device (default: " DEFAULT_ADSP ")");
MODULE_PARM_DESC(zswap, "Swap front and rear channel banks");
