QuadDSP 1.7
http://jengelh.medozas.de/projects/quad_dsp/


Introduction
============

Many Linux applications are not designed or simply do not support
output of more than 2 channels. For those who can do more than two
channels over OSS are struck down by the ALSA OSS emulation, which only
supports two channels. What the OSS emulation does is to register two
independent device nodes /dev/dsp0 and /dev/adsp0 for front and rear.
The QuadDSP driver is meant to unify the dsp and adsp devices and
provide one "true 4-channel" device.

This Kernel module is useful because not every soundcard supports
these features at the hardware level.


Developer warning
=================
This module violates at least two of the standard kernel practices.
(Namely using floating point and opening files from kernel-space)


Operation modes
===============
Since not every filter can be used for any number of channels, it
must fall back to some different strategy. Here is the matrix:

              Channels
Device        1     2       3 and more
Qdsp_diff     dup   diff    diff
Qdsp_dup      dup   dup     split
Qdsp_dsr      dup   dsr     split
Qdsp_dxr      dup   dxr     split
Qdsp_dpl2     dup   dpl2    split
Qdsp_qs       dup   qs      qs
Qdsp_sq       dup   sq      sq


4X2 splexer (split)
===================
QuadDSP provides an OSS-compatible interface at the device node
/dev/Qdsp_dup which is capable of taking SNDCTL_DSP_CHANNEL ioctls
with more than 2 chans. (Also accepts mono and stereo, though.) It
splits the input data and feeds half of the data to dsp0, the other
half to adsp0.

If, for example, you are setting 6 channels, 3 will go to the front
and 3 will go to the back. But then again, dsp0/adsp0 usually support
only 2, and will play only 2.


2T4 duplexer (dup)
==================
In case /dev/Qdsp_dup is set to mono or stereo, it duplicates the
front to the back. Some audio cards have a hardware-based "Duplicate"
control (such as CS46XX), but which sounds somewhat different.


Channel differential (diff)
===========================
Calculates the difference to the left/right side channel and uses
that as new L/R values for the rear.


2T3 Dolby Surround (dsr)
========================
QuadDSP provides a /dev/Qdsp_dsr device which, when 2 channels are
set, will decode the surround channel from the front-left and
front-right according to the Dolby matrices and feed it to both of
the rear channels. (The center channel is not used.)

Sounds good, but may generate too much echo feedback if the input has
asymmetric sound. This problem has been addressed (by Dolby) in the
Pro Logic II decoding.

Qdsp_dxr is a modification by me to the dsr algorithm - the left and
right channels are subtracted by the surround channel.


2T4 Dolby Pro Logic II (dpl2)
=============================
QuadDSP provides a /dev/Qdsp_dpl2 device which, when 2 channels are
set, will decode the rear left and rear right channels from the
front. (Center is unused.)


2T4 Quadrophonic Stereo (qs)
============================
Applies the QS matrix to the rear channel.


2T4 Surround Quadrophonic (sq)
==============================
Applies the SQ matrix to the rear channel.


Module options
==============
front=STRING      Path to the front device, defaults to /dev/dsp0
rear=STRING       Path to the rear device, defaults to /dev/adsp0

These parameters can be changed while the module is loaded by
changing the contents of the files in
/sys/module/quad_dsp/parameters/ . It only affects new file
descriptors that are opened, currently opened fds to Qdsp_* are not
changed.

Changing the following parameters has instant effect:

zswap=0|1         Swap front/rear channel banks


Compile-time options
====================
In the Makefile, you can select whether you want to use floating point
or integer arithmetic. Either you pick the line with -mno-soft-float
which will enable use of the FPU, or pick the one with -DINTEGER_ARITH
to go with the integer version.


Notes
=====
The module can be loaded without any sound modules or hardware
present.

Opening any of /dev/Qdsp_* will fail if either of the real front or
rear device cannot be opened. In case you get permission denied,
check the perms of all three /dev/Qdsp_*, /dev/dsp0 and /dev/adsp0
(or whatever you set for front and rear devs).

Be sure to adjust your udev configuration so that normal users can
also use the QuadDSP device nodes.

A sample udev file, 90-quad_dsp.rules, can be put into
/etc/udev.d/rules/ to automatically chown the /dev/Qdsp_* files to
root:audio and mode 0660. You can change that at will. /dev/dsp will
default to /dev/Qdsp_dpl2.

With applications which support more than two channels over ALSA,
it's all ok - QuadDSP is not needed in this case. But you might want
to use it anyway because of the DSR/DPL2 decoder.
